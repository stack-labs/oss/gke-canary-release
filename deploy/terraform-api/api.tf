resource "google_project_service" "enable_gce_api" {
  service = "compute.googleapis.com"

  disable_on_destroy         = true
  disable_dependent_services = true
}

resource "google_project_service" "enable_cloudbuild_api" {
  service = "cloudbuild.googleapis.com"

  disable_on_destroy         = true
  disable_dependent_services = true
}

resource "google_project_service" "enable_networking_api" {
  service = "servicenetworking.googleapis.com"

  disable_on_destroy         = true
  disable_dependent_services = true
}

resource "google_project_service" "enabled_container_api" {
  service = "container.googleapis.com"

  disable_on_destroy         = true
  disable_dependent_services = true
}

resource "google_project_service" "enabled_secretmanager_api" {
  service = "secretmanager.googleapis.com"

  disable_on_destroy         = true
  disable_dependent_services = true
}

resource "google_project_service" "enabled_artifactregistry_api" {
  service = "artifactregistry.googleapis.com"

  disable_on_destroy         = true
  disable_dependent_services = true
}
