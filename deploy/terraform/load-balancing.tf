resource "google_compute_firewall" "fw_allow_health_check" {
  name          = "fw-allow-health-check-${var.gcp_project_id}"
  direction     = "INGRESS"
  network       = module.vpc.network_id
  source_ranges = ["130.211.0.0/22", "35.191.0.0/16"]
  allow {
    protocol = "TCP"
    ports    = ["30080"]
  }
  target_tags = ["gke"]
}

resource "google_compute_global_address" "chameau_lb_ip" {
  name = "gke-lb-ip"
}

resource "google_compute_global_forwarding_rule" "http" {
  name = "gke-http-forwarding-rule"

  ip_protocol           = "TCP"
  load_balancing_scheme = "EXTERNAL_MANAGED"
  port_range            = "80"
  target                = google_compute_target_http_proxy.http_proxy.id
  ip_address            = google_compute_global_address.chameau_lb_ip.self_link
}

resource "google_compute_instance_group_named_port" "http_port" {
  group = module.gke.instance_group_urls[0]
  zone  = var.gcp_zone

  name = "http"
  port = 30080
}

resource "google_compute_health_check" "nginx-http" {
  name = "nginx-healthcheck-30080"

  check_interval_sec = 1
  timeout_sec        = 1

  http_health_check {
    request_path = "/healthz"
    port         = "30080"
  }
}

resource "google_compute_backend_service" "gke" {
  name                  = "gke-backend-http"
  port_name             = "http"
  protocol              = "HTTP"
  timeout_sec           = 100
  load_balancing_scheme = "EXTERNAL_MANAGED"

  dynamic "backend" {
    for_each = module.gke.instance_group_urls
    content {
      group           = backend.value
      balancing_mode  = "UTILIZATION"
      capacity_scaler = 1.0
      max_utilization = 0.8
    }
  }
  health_checks = [google_compute_health_check.nginx-http.id]
}

resource "google_compute_url_map" "chameau" {
  name = "gke-chameau-url-map"

  default_service = google_compute_backend_service.gke.id

  path_matcher { # TODO Retirer ?
    name            = "gke"
    default_service = google_compute_backend_service.gke.id
  }
}

resource "google_compute_target_http_proxy" "http_proxy" {
  name    = "gke-chameau-http-proxy"
  url_map = google_compute_url_map.chameau.id
}
