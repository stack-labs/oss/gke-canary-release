resource "google_artifact_registry_repository" "container_images_registry" {
  location      = var.gcp_region
  repository_id = "chameau-repository"
  description   = "Container image registry for the Chameau demonstration"
  format        = "DOCKER"
}
