module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "~> 8.1"

  project_id   = var.gcp_project_id
  network_name = "gke-network"
  routing_mode = "GLOBAL"

  subnets = [
    {
      subnet_name           = "gke-subnetwork"
      subnet_ip             = "10.10.20.0/24"
      subnet_region         = var.gcp_region
      description           = "Subnet for GKE resources"
      subnet_private_access = true
    }
  ]

  secondary_ranges = {
    "gke-subnetwork" = [
      {
        range_name    = "gke-services"
        ip_cidr_range = "10.11.0.0/16"
      },
      {
        range_name    = "gke-pods"
        ip_cidr_range = "10.12.0.0/16"
      },
    ]
  }
}

module "cloud-nat" {
  source        = "terraform-google-modules/cloud-nat/google"
  version       = "~> 5.0"
  project_id    = var.gcp_project_id
  region        = var.gcp_region
  create_router = true
  router        = "gke-router"
  network       = module.vpc.network_name
}
