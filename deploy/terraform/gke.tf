data "google_client_config" "default" {}

module "service_account_gke_nodes" {
  source     = "terraform-google-modules/service-accounts/google"
  version    = "~> 3.0"
  project_id = var.gcp_project_id
  prefix     = "chameau-sa"
  names      = ["gke-node"]
  project_roles = [
    "${var.gcp_project_id}=>roles/artifactregistry.reader",
    "${var.gcp_project_id}=>roles/logging.logWriter",
    "${var.gcp_project_id}=>roles/monitoring.metricWriter",
    "${var.gcp_project_id}=>roles/monitoring.viewer",
    "${var.gcp_project_id}=>roles/stackdriver.resourceMetadata.writer",
    "${var.gcp_project_id}=>roles/autoscaling.metricsWriter"
  ]
}

module "service_account_gke_app" {
  source     = "terraform-google-modules/service-accounts/google"
  version    = "~> 3.0"
  project_id = var.gcp_project_id
  prefix     = "chameau-sa"
  names      = ["gke-app"]
  project_roles = [
    "${var.gcp_project_id}=>roles/secretmanager.secretAccessor",
  ]
}

module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/beta-private-cluster"
  project_id                 = var.gcp_project_id
  name                       = "gke-${var.gcp_project_id}-${var.gcp_region}"
  regional                   = false
  zones                      = [var.gcp_zone]
  network                    = module.vpc.network_name
  subnetwork                 = module.vpc.subnets["${var.gcp_region}/gke-subnetwork"].name
  ip_range_pods              = module.vpc.subnets_secondary_ranges[0][0].range_name
  ip_range_services          = module.vpc.subnets_secondary_ranges[0][1].range_name
  http_load_balancing        = false
  network_policy             = false
  horizontal_pod_autoscaling = true
  filestore_csi_driver       = false
  enable_private_nodes       = true
  master_ipv4_cidr_block     = "10.0.0.0/28"
  create_service_account     = false
  service_account            = module.service_account_gke_nodes.service_account.email
  datapath_provider          = "ADVANCED_DATAPATH"
  remove_default_node_pool   = true
  deletion_protection        = false

  node_pools = [
    {
      name               = "node-pool-01"
      machine_type       = "e2-medium"
      node_locations     = var.gcp_zone
      min_count          = 1
      max_count          = 2
      local_ssd_count    = 0
      spot               = true
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS_CONTAINERD"
      enable_gcfs        = false
      enable_gvnic       = false
      logging_variant    = "DEFAULT"
      auto_repair        = true
      auto_upgrade       = true
      preemptible        = false
      initial_node_count = 1
    },
  ]

  node_pools_oauth_scopes = {
    node-pool-01 = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    node-pool-01 = {
      env = "chameau"
    }
  }

  node_pools_tags = {
    node-pool-01 = ["gke"]
  }
}

resource "google_service_account_iam_member" "gke_app_wip" {
  service_account_id = module.service_account_gke_app.service_account.id
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${var.gcp_project_id}.svc.id.goog[demo/read-sa]"
}

resource "google_service_account_iam_member" "gke_write_app_wip" {
  service_account_id = module.service_account_gke_app.service_account.id
  role               = "roles/iam.workloadIdentityUser"
  member             = "serviceAccount:${var.gcp_project_id}.svc.id.goog[demo/write-sa]"
}

provider "kubernetes" {
  host                   = "https://${module.gke.endpoint}"
  token                  = data.google_client_config.default.access_token
  cluster_ca_certificate = base64decode(module.gke.ca_certificate)
}
