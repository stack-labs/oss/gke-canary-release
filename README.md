# Déploiement *canari* sur Kubernetes

Ce dépôt contient du matériel de démonstration.

Ses auteurs sont :
- Antoine Aubé (<courriel@arjca.fr>) ;
- Kévin Viala (<kevin.viala@stack-labs.com>).

## Sujet

Notre système délivre une page Web avec un chameau dessiné en *ASCII Art*. Cette page est générée par un serveur appelé *chameau*, et une instance de *NGINX* équilibre la charge entre les instances de *chameau*. Ce système est déployé sur *Google Kubernetes Engine* grâce à *Skaffold*.

Nous voulons mettre à jour *chameau*. En effet, la couleur du fond n'est pas à notre goût, une correction doit être faite. Néanmoins, la mise à jour doit être effectuée sans déni de service, et doit être annulée en cas de problème. Pour cela, nous allons procéder à un déploiement *canari* : la nouvelle version du serveur Web va cohabiter un temps avec l'ancienne version, et un pourcentage des requêtes au serveur Web lui seront affectées. Si ces requêtes sont traitées avec succès (ce que nous détectons, ici, avec un code de réponse *HTTP* différent de *5XX*), alors nous pouvons augmenter le pourcentage de requêtes traitées par la nouvelle version jusqu'à tout lui affecter. Nous allons augmenter le pourcentage progressivement, jusqu'à arriver à cinquante pourcents. Dès lors, l'ancienne version pourra être éteinte. Cette mécanique est automatisée par *Flagger*.

## Prérequis

### Google Cloud Platform

Cette démonstration utilise *Google Kubernetes Engine* (*GKE*). Il vous faut donc disposer d'un compte *Google Cloud Platform* (*GCP*) et d'un projet *GCP* sur lesquels vous avez le rôle d'*Administrateur* (nous allons activer des *API*, créer des ressources et des comptes de service).

Sur le poste de démonstration, l'outil en ligne de commande *gcloud* est nécessaire. Il faut préalablement s'authentifier avec ; pour cela, exécutez les commandes suivantes : `gcloud auth login`, `gcloud auth application-default login` et `gcloud config set project PROJECT-ID` (remplacer `PROJECT_ID` par l'identifiant du projet dans lequel vous allez déployer le système). De plus, il faut installer l'extension `gke-gcloud-auth-plugin`.

### Skaffold

La construction des images de conteneurs et leur déploiement est orchestré par l'outil [*Skaffold*](https://skaffold.dev/). Veillez à l'installer sur le poste de démonstration. Les images de conteneurs sont construites localement avec [*Docker*](https://www.docker.com/), l'installer est donc également nécessaire.

### Nom de domaine

Un nom de domaine est nécessaire pour interroger le serveur. L'*Ingress Controller* se sert du nom de domaine dans la requête pour assigner les requêtes au bon conteneur.
Les enregistrements *DNS* de ce nom de domaine seront édités pour la démonstration.

### Terraform

Pour le déploiement avec *Terraform*, trois valeurs sont requises : `gcp_project_id`, `gcp_region` et `gcp_zone`. Elles peuvent être fournies dans le terminal au moment d'appliquer le plan de déploiement, ou être renseignées dans un fichier *values.tfvars* dans *deploy/terraform* et *deploy/terraform-api*.

### Kubernetes

Pour interagir avec la grappe de serveurs *Kubernetes*, il est nécessaire également de disposer de l'outil *kubectl*.
De même, la construction de l'image de conteneur et son déploiement est réalisé depuis le poste de démonstration ; ici, nous utilisons *docker* et *skaffold*.

## Étapes de la démonstration

### Étapes préliminaires

Étant donné la durée de mise en place (le déploiement de l'infrastructure peut durer une dizaine de minutes), quelques étapes peuvent être anticipées :

1. Parcourir le [code source de l'application](cmd/chameau/main.go) et le [modèle de page *HTML*](web/template/chameau.html)  : le serveur sert une page *HTML* très simple avec un chameau dessiné en *ASCII Art*, et un fond beige ;
2. Si le projet est neuf, activer les *API* de *GCP* avec `cd deploy/terraform-api && terraform init && terraform apply -auto-approve` ;
3. Déployer l'infrastructure : exécuter `cd deploy/terraform-api && terraform init && terraform apply -auto-approve -var-file=PATH-TO-TFVARS-FILE` (remplacez `PATH-TO-TFVARS-FILE` par le chemin vers le fichier *values.tfvars* que vous avez préparé) ;
4. S'authentifier à la grappe de serveurs *Kubernetes* : `gcloud container clusters get-credentials CLUSTER_NAME --region=LOCATION` (remplacez `CLUSTER_NAME` par le nom de la grappe générée par *Terraform*, et `LOCATION` par le nom de la **zone** de déploiement) ;
5. Mettre à jour les valeurs des fichiers pour le déploiement avec *Kubernetes* :
   - Créer un fichier *skaffold.env* dans le dossier contenant *skaffold.yaml* avec deux valeurs : `GCP_SERVICE_ACCOUNT` (adresse électronique du compte de service créé par *Terraform*) et `HOST_DOMAIN` (le nom de domaine auquel le serveur est contacté) ;
   - Mettre à jour le nom de domaine à la fin du [fichier de configuration de *Flagger*](deploy/kubernetes/canary.yaml) (ligne 56).
6. Mettre à jour les enregistrements *DNS* du domaine que vous avez choisi ;
7. Configurer le dépôt d'artefacts pour *Skaffold* : `skaffold config set default-repo [REGISTRY]` (remplacez `REGISTRY` par l'identifiant que vous pouvez trouver dans la console *GCP* dans l'interface du service *Artifact Registry*).

### Déploiement initial

1. Se rendre sur l'*URL* de la page Web : actuellement, elle renvoie un message d'erreur. En effet, le serveur *chameau* n'est pas déployé ;
2. Afficher les *pods* de la grappe de serveurs : `kubectl get pods`. Il n'y a pour l'instant aucun conteneur exécuté ;
3. Déployer le manifeste *Kubernetes* avec *Skaffold* : `skaffold run` ;
4. Se rendre à l'*URL* de la page Web : un chameau y est dessiné en *ASCII Art* ;
5. Afficher les *pods* de la grappe de serveurs : `kubectl get pods`. Il y pour l'instant six *pods* : 
   - Deux sont préfixés *chameau-primary-...* : il s'agit du serveur de la page Web qui est présent en deux exemplaires, d'après le manifeste *Kubernetes* ;
   - Un est préfixé *ingress-nginx-...* : il s'agit de l'instance de *NGINX* qui réceptionne les requêtes et les dirige vers les instances de *chameau* ;
   - Trois sont préfixés *flagger-...* : il s'agit des conteneurs liés à *Flagger*, notamment pour la supervision de *NGINX* (stockage dans *flagger-prometheus-...*) et le test des instances *canari* lors d'un déploiement (*flagger-loadtester-...*).

### Déploiement d'une nouvelle version

1. Modifier le [code source de *chameau*](cmd/chameau/main.go) (une suggestion est donnée dans les premières lignes du fichier), sauvegarder ;
2. Redéployer avec *Skaffold* : `skaffold run` ;
3. Afficher les *pods* de la grappe de serveurs : `kubectl get pods`. En plus des conteneurs *chameau-primary-...*, deux nouveaux conteneurs *chameau-...* sont apparus : ces conteneurs exécutent la nouvelle version de *chameau* ;
3. Observer le déploiement. Nous suggérons de couper l'écran en deux :
   - D'un côté, ouvrir un terminal et afficher les traces d'exécution du conteneur *Flagger* : `kubectl logs -f flagger-...`. Au fur et à mesure, des lignes vont tracer le passage progressif du trafic vers le conteneur *canari* ;
   - De l'autre côté, ouvrir un navigateur Web et charger à répétition l'*URL* de la page Web : plus le temps passe, plus la fréquence de la page Web corrigée augmente.
4. Quand le déploiement est complété (c.-à-d. il n'y a plus que la nouvelle version qui est chargée), afficher les *pods* de la grappe de serveurs : `kubectl get pods`. Il n'y a de nouveau plus que deux instances de *chameau-primary-...*.
