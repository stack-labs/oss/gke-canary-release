// chameau -- a simple Web server
// Copyright (C) 2024  Antoine Aubé <courriel@arjca.fr>

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package main

import (
	"html/template"
	"net/http"
	"time"
)

// Background color of the HTML page
// Two alternatives are given, comment one out
const BACKGROUND_COLOR_HEX = "#EEE7DA"
// const BACKGROUND_COLOR_HEX = "#AFC8AD"

type CamelData struct {
	BackgroundColorHex string
	Date               time.Time
}

type Handler struct {
	camelTemplate *template.Template
}

func (h *Handler) handleCamel(w http.ResponseWriter, r *http.Request) {
	data := CamelData{}
	data.BackgroundColorHex = BACKGROUND_COLOR_HEX
	data.Date = time.Now()

	h.camelTemplate.Execute(w, data)
}

func main() {
	tmpl := template.Must(template.ParseFiles("web/template/chameau.html"))
	handler := Handler{tmpl}

	http.HandleFunc("/", handler.handleCamel)

	http.ListenAndServe(":80", nil)
}
