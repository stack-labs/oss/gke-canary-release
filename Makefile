chameau.exe:
	@CGO_ENABLED=0 GOOS=linux go build \
		-a \
		-installsuffix cgo \
		-o chameau.exe \
		-v cmd/chameau/main.go

container-image:
	docker build \
		-t arjca/chameau:latest \
		-f build/Containerfile.chameau \
		.

run-local:
	docker run \
		-p 8080:8080 \
		--rm \
		arjca/chameau:latest

clean:
	rm chameau

api-activate:
	cd deploy/terraform-api && terraform apply -auto-approve

infrastructure-start:
	cd deploy/terraform && terraform apply -auto-approve
